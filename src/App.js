import React from 'react';
import Axios from 'axios';

const API_BASE = 'http://localhost:3001';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      notes: [],
    };

    this.inputRef = React.createRef();
  }

  async onRefreshButtonClick() {
    console.log('refresh');

    const {data: notes} = await Axios.get(API_BASE + '/note/list');
    console.log(notes);
    this.setState({notes});
  }

  async onAddButtonClick() {
    console.log('add', this.inputRef.current.value);

    await Axios.post(API_BASE + '/note/create', {
      text: this.inputRef.current.value,
    });
    const {data: notes} = await Axios.get(API_BASE + '/note/list');
    this.setState({notes});
    
    this.inputRef.current.value = '';
  }

  render() {
    return <div>
      <div>
        <button onClick={this.onRefreshButtonClick.bind(this)}>refresh</button>
      </div>
      <div>
        <input ref={this.inputRef} type="text"/>
        <button onClick={this.onAddButtonClick.bind(this)}>add</button>
      </div>

      <ul>
        {this.state.notes.map(note => <li key={note}>{note}</li>)}
      </ul>
    </div>;
  }
}

export default App;
